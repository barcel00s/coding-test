<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //Create customers table
        Schema::create('customers', function (Blueprint $table) {
            $table->increments('id');
            $table->string('code')->unique();
            $table->string('name');
            $table->date('since');
            $table->float('revenue',11,2);
            $table->timestamps();
        });

        //Create products table
        Schema::create('products', function (Blueprint $table) {
            $table->increments('id');
            $table->string('code')->unique();
            $table->string('description');
            $table->integer('category');
            $table->float('price',11,2);
            $table->timestamps();
        });


        //Create discounts table
        Schema::create('discounts', function (Blueprint $table) {
            $table->increments('id');
            $table->string('description');
            $table->float('from',11,2);
            $table->float('to',11,2)->nullable();
            $table->float('value',11,2);

            //Percentage discount or extra product
            $table->enum('operator',['%','+']);

            //Discount based on quantity or value
            $table->enum('type',['Quantity','Value']);

            //Discount applies to the cheapest product, the most expensive or the total order
            $table->enum('applies_to',['On cheapest item', 'On most expensive item', 'On total order']);

            $table->timestamps();

        });

        //Create discount_product table
        Schema::create('discount_product', function (Blueprint $table) {

            $table->integer('discount_id')
                ->unsigned();

            $table->foreign('discount_id')
                ->references('id')
                ->on('discounts')
                ->onDelete('cascade');

            $table->integer('product_id')
                ->unsigned();

            $table->foreign('product_id')
                ->references('id')
                ->on('products')
                ->onDelete('cascade');

        });

        //Create orders table
        Schema::create('orders', function (Blueprint $table) {
            $table->increments('id');
            $table->string('code')->unique();
            $table->float('total',11,2)->default(0.00);
            $table->float('discount_value',11,2)->default(0.00);
            $table->timestamps();

            $table->integer('customer_id')
            ->unsigned();

            $table->foreign('customer_id')
                ->references('id')
                ->on('customers')
                ->onDelete('cascade');
        });

        //Create discount_order table
        Schema::create('discount_order', function (Blueprint $table) {

            $table->integer('discount_id')
                ->unsigned();

            $table->foreign('discount_id')
                ->references('id')
                ->on('discounts')
                ->onDelete('cascade');

            $table->integer('order_id')
                ->unsigned();

            $table->foreign('order_id')
                ->references('id')
                ->on('orders')
                ->onDelete('cascade');

        });

        //Create order_product table
        Schema::create('order_product', function (Blueprint $table) {

            $table->integer('quantity');
            $table->float('price',11,2);
            $table->float('discount_value',11,2)->default(0.00);
            $table->integer('extra_products')->default(0);

            $table->integer('order_id')
                ->unsigned();

            $table->foreign('order_id')
                ->references('id')
                ->on('orders')
                ->onDelete('cascade');

            $table->integer('product_id')
            ->unsigned();

            $table->foreign('product_id')
                ->references('id')
                ->on('products')
                ->onDelete('cascade');

        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

        //Drop table discount_order
        Schema::drop('discount_order');

        //Drop table discount_product
        Schema::drop('discount_product');

        //Drop table discounts
        Schema::drop('discounts');

        //Drop table order_product
        Schema::drop('order_product');

        //Drop table orders
        Schema::drop('orders');

        //Drop table customers
        Schema::drop('customers');

        //Drop table products
        Schema::drop('products');

    }
}
