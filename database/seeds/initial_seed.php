<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Facades\Log;

class initial_seed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        //Load customers
        $path = storage_path() . "/app/customers.json";
        $customers = json_decode(file_get_contents($path), true);

        foreach($customers as $customerData){

            $customerID = $customerData['id'];

            try{
                $customer = \App\Customer::query()
                    ->whereCode($customerID)
                    ->firstOrFail();
            }
            catch(ModelNotFoundException $ex){
                $customer = new \App\Customer();
            }

            $customer->updateCustomerWithData($customerData);
        }

        //Load products
        $path = storage_path() . "/app/products.json";
        $products = json_decode(file_get_contents($path), true);

        foreach($products as $productData){

            $productID = $productData['id'];

            try{
                $product = \App\Product::query()
                    ->whereCode($productID)
                    ->firstOrFail();
            }
            catch(ModelNotFoundException $ex){
                $product = new \App\Product();
            }

            $product->updateProductWithData($productData);

        }


        //Load discounts

        //1000€ - 10% discount on total order
        $discountData = [
            'description' => '10% discount on total order (1000€ or more)',
            'from'        => 1000.00,
            'to'          => null,
            'value'       => 0.10,
            'operator'    => '%',
            'type'        => 'Value',
            'applies_to'   => 'On total order',
            'product_ids'  => []
        ];

        $discount = new \App\Discount();
        $discount->updateDiscountWithData($discountData);

        //Switches - 1 extra for each 5
        $discountData = [
            'description' => 'Extra switch for every 5 switches',
            'from'        => 5.00,
            'to'          => null,
            'value'       => 1.00,
            'operator'    => '+',
            'type'        => 'Quantity',
            'applies_to'  => 'On total order',
            'product_ids'  => ['B101','B102','B103']
        ];

        $discount = new \App\Discount();
        $discount->updateDiscountWithData($discountData);

        //Tools - 20% discount on cheapest product for buying 2 or more
        $discountData = [
            'description' => '20% discount on cheapest product (2 or more tools)',
            'from'        => 2.00,
            'to'          => null,
            'value'       => 0.20,
            'operator'    => '%',
            'type'        => 'Quantity',
            'applies_to'   => 'On cheapest item',
            'product_ids'  => ['A101','A102']
        ];

        $discount = new \App\Discount();
        $discount->updateDiscountWithData($discountData);


        //Load orders
        $path = storage_path() . "/app/orders.json";
        $orders = json_decode(file_get_contents($path), true);

        foreach($orders as $orderData){

            $orderID = $orderData['id'];

            try{
                //Delete order before recreating it
                \App\Order::query()
                    ->whereCode($orderID)
                    ->delete();
            }
            catch(ModelNotFoundException $ex){
                //
            }

            $order = new \App\Order();
            $order->updateOrderWithData($orderData);

        }
    }
}
