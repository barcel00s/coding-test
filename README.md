# Order discounts API #

### Description  ###

This is an API created to receive purchase orders and apply pre-determined discounts to them.

For the moment these are the available discounts:

- 10% discount on the total order if order (after individual products discounts) is of 1000� or more

- Extra switch for every 5 switches purchased (products of category 2)

- 20% discount on cheapest product if order has 2 or more tools (products of category 1)



API is also ready to accept other types of discounts.



Here are some examples of discounts that can be created and processed:
 
 - % discount on orders above or between pre-determined amounts.
 
 - % discount on orders with total number of items purchased above or between a pre-determined quantity.
 
 - Extra products (of the same kind or not) on orders above or between pre-determined amounts.
 
 - Extra products (of the same kind or not) on orders with total number of items purchased above or between a pre-determined quantity.
 
 - % discount on cheapest product or on most expensive product if product order is above or between pre-determined amounts.
 
 - Extra items on cheapest product or on most expensive product if product order is above or between pre-determined amounts.
 
 - % discount on cheapest product or on most expensive product if order has enough items of a selected product or is between a certain quantity.
 
 - Extra items on cheapest product or on most expensive product if order has enough items of a selected product or is between a certain quantity.
 

### Input ###

API accepts orders via POST method with json parameter holding the order in the following format:

    [{  
        "id": "1",
        "customer-id": "1",
        "items": [
        {
          "product-id": "B102",
          "quantity": "10",
          "unit-price": "4.99",
          "total": "49.90"
        }
    ],
    "total": "49.90"
    },
    {
        "id": "2",
        "customer-id": "2",
        "items": [
        {
          "product-id": "B102",
          "quantity": "5",
          "unit-price": "4.99",
          "total": "24.95"
        }
        ],
    "total": "24.95"
    }]


API will then return the same order but with the discounts applied, at product level, in case the discount applies to the product, or at order level, in case the discount applies globally to the order (for example, order value above a certain amount).

It will also return the total original payment amount of the order, the total discount value and the final value of the order after discounts.

### Output ###


    [{
        "order-id": "1",
        "customer-id": "1",
        "items": [
            {
                "product-id": "B102",
                "quantity": 10,
                "unit-price": 4.99,
                "product-discounts": [
                    {
                        "discount-id": 2,
                        "description": "Extra switch for every 5 switches",
                        "discount-value": 0,
                        "extra-products-offered": 2
                    }
                ],
                "total": 49.9,
                "total-with-discounts": 49.9
            }
        ],
        "order-level-discounts": [],
        "total": 49.9,
        "total-with-discounts": 49.9
    },
    {
        "order-id": "2",
        "customer-id": "2",
        "items": [
            {
                "product-id": "B102",
                "quantity": 5,
                "unit-price": 4.99,
                "product-discounts": [
                    {
                        "discount-id": 2,
                        "description": "Extra switch for every 5 switches",
                        "discount-value": 0,
                        "extra-products-offered": 1
                    }
                ],
                "total": 24.95,
                "total-with-discounts": 24.95
            }
        ],
        "order-level-discounts": [],
        "total": 24.95,
        "total-with-discounts": 24.95
    }]

N.B. If an order has a product that is entitled to a discount and, after applying that discount, the final value of the order is also entitled to another discount, the product discount will be applied first and the order discount aftwards, on top of that. Example: ((1000 - 10%) - 10%) = 810.


### Setup API ###

API was built using Laravel 5.6 and therefore needs to have PHP 7.2 and MySQL installed.
To install it you only need to:

- download the repository into a folder called coding-test in your localhost server

- run the following commands with root on mysql server:

```
    CREATE DATABASE codingTest;    
    CREATE USER 'codingTest'@'localhost' IDENTIFIED BY 'codingTest';    
    GRANT ALL PRIVILEGES ON codingTest.* TO 'codingTest'@'localhost';
```

- run 'composer update' on project root

- run 'php artisan migrate --seed' on project folder.

- send POST requests to localhost/coding-test/orders to add order or access localhost/coding-test/orders to check current orders


### Technical details ###

When a discount is created it can be associated to multiple products as well as multiple orders. 
By associating it to an order, this discount will be applied to the total value of the order or the total number of items on it. If we associate the discount to a product, it will be applied to the total value of the price of the product order regardless of the number of items of that product.

The order record field 'discount_value' reflects the discounts obtained on all products within the order as well as the discounts associated to the order itself.

For example, let's say we have an order that has a discount of 10% on switches because the customer bought 5 switches, and in total (after this discount) costs 1000� thus getting a 10% extra on the whole order. The 'discount_value' field will reflect the discount given to the switches + the discount given on the total order. 

The discount table has the following 6 fields:

 - From: The amount/quantity after which the discount is applicable, regardless if it is an order or a product;
 
 - To: The amount/quantity until which the discount is applicable, regardless if it is an order or a product. This value can be null;
 
 - Value: The value of the discount. If the discount is a cut on the full price, this value has to be between 0 and 1 to reflect a percentage. If the discount consists of an extra item, it has to be a positive value.
 - Operator: Can be either '%', in case the discount is a cut on the price, or '+' in case it consists of offering extra products;
 
 - Type: It can be either 'Value', in case it depends on the order/product purchase amount value or 'Quantity' in case it depends on the number of items purchased;
 
 - Applies_to: Can be 'On total order', in case the discount is to be applied to the order itself, 'On cheapest item', in case it will be applied to the cheapest product or 'On most expensive item', in case it will be applied to the most expensive item. 

### Contact ###
If you have any questions regarding this project, feel free to contact [me](mailto:rocardoso@gmail.com).

Rui Cardoso