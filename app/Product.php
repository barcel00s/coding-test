<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [

    ];

    /**
     * @param $productData - Data for the product
     *
     *      Configure product
     *
     * Example:
     *      {
     *          "id": "B101",
     *          "description": "Basic on-off switch",
     *          "category": "2",
     *          "price": "4.99"
     *      },
     *
     * @return null
     */

    public function updateProductWithData($productData){

        $this->setAttribute('code',$productData['id']);
        $this->setAttribute('description',$productData['description']);
        $this->setAttribute('category',$productData['category']);
        $this->setAttribute('price',$productData['price']);

        $this->save();
    }

    /**
     * Get the orders associated to this product
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function orders()
    {
        return $this->belongsToMany('App\Order')
            ->withPivot('quantity','price','discount_value','extra_products');
    }

    /**
     * Get the discounts associated to this product
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function discounts()
    {
        return $this->belongsToMany('App\Discount');
    }

}
