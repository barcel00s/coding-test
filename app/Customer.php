<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [

    ];

    /**
     * @param $customerData - Data for the customer
     *
     *      Configure customer
     *
     * Example:
     *      {
     *          "id": "3",
     *          "name": "Jeroen De Wit",
     *          "since": "2016-02-11",
     *          "revenue": "0.00"
     *      },
     *
     * @return null
     */

    public function updateCustomerWithData($customerData){

        $this->setAttribute('code',$customerData['id']);
        $this->setAttribute('name',$customerData['name']);
        $this->setAttribute('since',$customerData['since']);
        $this->setAttribute('revenue',$customerData['revenue']);

        $this->save();

    }

    /**
     * Returns the orders associated to this customer
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function orders()
    {
        return $this->hasMany('App\Order');
    }
}
