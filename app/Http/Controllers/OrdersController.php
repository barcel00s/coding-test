<?php

namespace App\Http\Controllers;

use App\Order;
use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Facades\Log;

class OrdersController extends Controller
{

    /**
     * Display a listing of all the orders.
     * @return array
     */

    public function index(){

        $orderRecords = Order::query()->get();

        $orders = [];

        foreach($orderRecords as $order){
            $orders[] = $this->print_order($order);
        }

        return $orders;
    }

    /**
     * Returns the order identified by @id
     *
     * @param $code
     * @return mixed
     */
    public function show($code){

        $order = Order::query()
            ->whereCode($code);

        if($order){
            return $this->print_order($order);
        }
        else{
            return null;
        }


    }

    /**
     * Creates a new order and returns it.
     *
     * @param Request $request
     * @return mixed
     */

    public function put(Request $request){

        $orders = json_decode($request->input('json'), true);

        $printOrders = [];

        foreach($orders as $orderData){

            $orderID = $orderData['id'];

            try{
                //Delete order before recreating it
                Order::query()
                    ->whereCode($orderID)
                    ->delete();
            }
            catch(ModelNotFoundException $ex){
                //
            }

            $order = new Order();
            $order->updateOrderWithData($orderData);

            $printOrders[] = $this->print_order($order);
        }

        return $printOrders;
    }

    /**
     * Format the order to return it to client
     *
     * @param $order
     * @return array
     */
    private function print_order($order){

        $items = [];

        foreach($order->products as $productOnOrder){

            $discounts = [];

            if ($productOnOrder->pivot->discount_value > 0 || $productOnOrder->pivot->extra_products > 0){

                $discountsForProduct = $productOnOrder->discounts;

                foreach($discountsForProduct as $discount){

                    $discounts[] = [
                        'discount-id'            => $discount->id,
                        'description'            => $discount->description,
                        'discount-value'         => round($productOnOrder->pivot->discount_value,2),
                        'extra-products-offered' => $productOnOrder->pivot->extra_products,
                    ];

                }

            }

            $items[] = [
                'product-id'              => $productOnOrder->code,
                'quantity'                => $productOnOrder->pivot->quantity,
                'unit-price'              => round($productOnOrder->price,2),
                'product-discounts'       => $discounts,
                'total'                   => round($productOnOrder->pivot->price,2),
                'total-with-discounts'    => round($productOnOrder->pivot->price,2)-round($productOnOrder->pivot->discount_value,2),
            ];
        }

        $discountsForOrder = $order->discounts;
        $discounts = [];

        foreach($discountsForOrder as $discount){

            $discounts[] = [
                'discount-id'           => $discount->id,
                'description'           => $discount->description,
                'total-discounts-value' => round($order->discount_value,2),
            ];

        }

        $orderPrint = [
            'order-id'              => $order->code,
            'customer-id'           => $order->customer->code,
            'items'                 => $items,
            'order-level-discounts' => $discounts,
            'total'                 => round($order->total,2),
            'total-with-discounts'  => round($order->total,2) - round($order->discount_value,2),
        ];

        return $orderPrint;
    }
}
