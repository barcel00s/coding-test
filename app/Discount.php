<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class Discount extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [

    ];

    /**
     * @param $discountData - Data for the discount
     *
     *      Configure discount
     *
     * Example:
     *          $discountData = [
     *              'description' => '20% discount on cheapest product (2 or more tools)',
     *              'from'        => 2.00,
     *              'to'          => null,
     *              'value'       => 0.20,
     *              'operator'    => '%',
     *              'type'        => 'Quantity',
     *              'applies_to'   => 'On cheapest item',
     *              'product_ids'  => ['A101','A102']
     *         ];
     *
     * @return null
     */

    public function updateDiscountWithData($discountData){

        $discount = $this;

        $discount->setAttribute('description',$discountData['description']);
        $discount->setAttribute('from',$discountData['from']);
        $discount->setAttribute('to',$discountData['to']);
        $discount->setAttribute('value',$discountData['value']);
        $discount->setAttribute('operator',$discountData['operator']);
        $discount->setAttribute('type',$discountData['type']);
        $discount->setAttribute('applies_to',$discountData['applies_to']);
        $discount->save();

        foreach($discountData['product_ids'] as $productCode){

            try{
                $product = Product::query()
                    ->whereCode($productCode)
                    ->firstOrFail();

                $discount->products()->attach($product->id);
            }
            catch(ModelNotFoundException $ex){
                //
            }

        }

    }

    /**
     * Get the products associated to this discount
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function products()
    {
        return $this->belongsToMany('App\Product');
    }

    /**
     * Get the orders associated to this discount
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function orders()
    {
        return $this->belongsToMany('App\Order');
    }

}
