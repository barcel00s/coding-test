<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Facades\Log;

class Order extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [

    ];

    /**
     *
     * Process the individual items discounts
     *
     * Discounts can be based on quantity or on value and apply
     * to the cheapest item on the order or the most expensive one.
     *
     * A discount can be to reduce the price on an individual item (percentage) or offer more items instead
     *
     */

    public function processDiscountOnSingleProducts(){

        //First check for products on the order that may apply to a discount
        $productsOnOrder = $this->products()->get();

        foreach($productsOnOrder as $product){

            if($product->discounts->count() > 0){

                $discounts = $product->discounts->all();

                foreach($discounts as $discount){

                    switch($discount->applies_to){

                        case 'On cheapest item';

                            $productSelected = $this->products()
                                ->orderBy('price')->first();

                            break;

                        case 'On most expensive item';

                            $productSelected = $this->products()
                                ->orderBy('price')->last();

                            break;

                        default;

                            $productSelected = $this->products()
                                ->orderBy('price')->first();

                            break;
                    }

                    switch($discount->type){
                        //Discount based on the value of the purchase for a specific item.

                        case 'Value';

                            switch($discount->operator){
                                case '%';

                                    if ($productSelected->pivot->discount_value == 0 && $product->pivot->price >= $discount->from) {

                                        //Discount is applied on the total order of the selected product
                                        $discountApplied = $productSelected->price * $productSelected->pivot->quantity * $discount->value;

                                        $this->products()->updateExistingPivot($productSelected->id,[
                                            'discount_value' => $discountApplied,
                                        ]);

                                    }
                                    break;

                                case '+';

                                    if ($productSelected->pivot->extra_products == 0 && $product->pivot->price >= $discount->from) {

                                        $numberOfExtraItems = ($product->pivot->quantity / $discount->from)
                                            * $discount->value;

                                        $this->products()->updateExistingPivot($product->id,
                                            ['extra_products' => $numberOfExtraItems]);
                                    }

                                    break;
                            }
                            break;

                        //Discount based on item order quantity
                        case 'Quantity';

                            switch($discount->operator){
                                case '%';

                                    if ($productSelected->pivot->discount_value == 0 && $product->pivot->quantity >= $discount->from) {

                                        //Discount is applied on the total order of the select product
                                        $discountApplied = $productSelected->price * $productSelected->pivot->quantity * $discount->value;

                                        $this->products()->updateExistingPivot($productSelected->id,[
                                            'discount_value' => $discountApplied,
                                        ]);

                                    }

                                    break;

                                case '+';

                                    if ($productSelected->pivot->extra_products == 0 && $product->pivot->quantity % $discount->from == 0) {
                                        $numberOfExtraItems = ($product->pivot->quantity / $discount->from)
                                            * $discount->value;

                                        $this->products()->updateExistingPivot($product->id,
                                            ['extra_products' => $numberOfExtraItems]);
                                    }

                                    break;
                            }

                            break;
                    }

                }
            }

        }

    }

    /**
     * Fetch discounts that apply to total order, based on quantity or on value
     *
     * Discounts can be based on quantity or on value and apply to the total order
     *
     * A discount can be to reduce the price on the total order value (percentage) or to offer more products to the customer
     *
     */

    public function processDiscountsOnTotalOrder(){

        //The discount on the total order will be applied on top of other individual item discounts
        $totalAmountOfOrderWithDiscounts = $this->getAttribute('total') - $this->getAttribute('discount_value');

        $discounts = Discount::query()
            ->whereDoesntHave('products')
            ->where(function($query) use ($totalAmountOfOrderWithDiscounts){
                $query
                    ->where(function($query) use ($totalAmountOfOrderWithDiscounts){
                    $query
                        ->where('type','Value')
                        ->where('from','<=',$totalAmountOfOrderWithDiscounts)
                        ->where(function($query) use ($totalAmountOfOrderWithDiscounts){
                            $query
                                ->where('to','>=',$totalAmountOfOrderWithDiscounts)
                                ->orWhere('to',null);
                        });
                })
                ->orWhere(function($query){
                    $query
                        ->where('type','Quantity')
                        ->where('from','<=',$this->calculateTotalNumberOfProductsOnOriginalOrder())
                        ->where(function($query){
                            $query
                                ->where('to','>=',$this->calculateTotalNumberOfProductsOnOriginalOrder())
                                ->orWhere('to',null);
                        });

                });
            })
            ->get();

        foreach($discounts as $discount){

            switch($discount->type) {
                case 'Value';

                    switch ($discount->operator) {

                        //Percentage discount
                        case '%';
                            //In case purchase value exceeds a certain amount we can offer him a discount on the total order
                            $discountValue = $totalAmountOfOrderWithDiscounts * $discount->value;
                            $total = $discountValue;

                            $this->setAttribute('discount_value', $total);

                            $this->discounts()->attach($discount->id);

                            break;

                        //Extra products
                        case '+';
                            //In case purchase value exceeds a certain amount we can offer the customer more items

                            $productsOnDiscount = $discount->products->all();

                            foreach($productsOnDiscount as $productOnDiscount)
                                //We now look for this product on the order to see if the discount is applicable

                                foreach($this->products()->get() as $productOnOrder){

                                    //If we have the product on the basket we apply the discount and add the number of
                                    //items we want to offer.
                                    if($productOnOrder->id == $productOnDiscount->id){
                                        $numberOfExtraItems = $productOnOrder->pivot->quantity + $discount->value;

                                        $this->products()->updateExistingPivot($productOnOrder->id,
                                            ['extra_products' => $numberOfExtraItems]);
                                    }
                                    else{
                                        //Product is not on the order yet so we add it.
                                        $this->products()->attach($productOnDiscount->id,[
                                            'quantity' => 0,
                                            'extra_products' => 1,
                                        ]);
                                    }

                                    $this->discounts()->attach($discount->id);
                                }

                            break;
                    }
                    break;

                case 'Quantity';
                    switch ($discount->operator) {

                        //Percentage discount
                        case '%';
                            //In case customer buys a certain amount of items we can offer him a discount on the total
                            //amount of the order

                            $totalAmountOfProductsOnOrder = $this->calculateTotalNumberOfProductsOnOriginalOrder();

                            if($totalAmountOfProductsOnOrder >= $discount->from){
                                $discountValue = $totalAmountOfOrderWithDiscounts * $discount->value;
                                $total = $discountValue;

                                $this->setAttribute('discount_value', $total);

                                $this->discounts()->attach($discount->id);
                            }

                            break;

                        //Extra products
                        case '+';
                            //In case customer buys a certain amount of items we can offer him another item

                            $productsAssociatedToDiscount = $discount->products()->get();

                            foreach ($productsAssociatedToDiscount as $product) {

                                //Check if we have this product on the order and the amount ordered
                                //is enough for the discount

                                $productIsOnOrder = $this->products()
                                    ->whereId($product->id)
                                    ->wherePivot('quantity','>=',$discount->from)
                                    ->exists();

                                if($productIsOnOrder){
                                    //This product on the order applies to this discount
                                    //We now check how many items we can offer the user

                                    $productOnOrder = $this->products()
                                        ->whereId($product->id)
                                        ->wherePivot('quantity','>=',$discount->from)->first();

                                    if ($productOnOrder->pivot->quantity % $discount->from == 0) {
                                        $numberOfExtraItems = ($productOnOrder->pivot->quantity / $discount->from)
                                                                * $discount->value;

                                        $this->products()->updateExistingPivot($productOnOrder->id,
                                            ['extra_products' => $numberOfExtraItems]);
                                    }
                                    else{
                                        //Product is not on the order yet so we add it.
                                        $this->products()->attach($product->id,[
                                            'quantity' => 0,
                                            'extra_products' => 1,
                                        ]);
                                    }

                                    $this->discounts()->attach($discount->id);
                                }

                            }

                            break;

                    }

                    break;
            }

        }

        $this->save();
    }

    /**
     * @param $orderData - Data for the order
     *
     *      Configure order
     *
     * Example:
     *   {
     *      "id": "2",
     *      "customer-id": "2",
     *      "items": [
     *          {
     *          "product-id": "B102",
     *          "quantity": "5",
     *          "unit-price": "4.99",
     *          "total": "24.95"
     *          }
     *      ],
     *      "total": "24.95"
     *  }
     *
     * @return null
     */

    public function updateOrderWithData($orderData){

        $order = $this;

        $order->setAttribute('code',$orderData['id']);

        $customerID = $orderData['customer-id'];

        try{
            $customer = Customer::query()
                ->whereCode($customerID)
                ->firstOrFail();

            $order->customer()->associate($customer);
        }
        catch(ModelNotFoundException $ex){
            Log::error('Customer '.$customerID.' not found for order '.$orderData['id']);

            return null;
        }

        $order->setAttribute('total',$orderData['total']);
        $order->save(); //We need to save the order before adding products to it.

        $items = $orderData['items'];

        foreach($items as $itemData){

            $productID = $itemData['product-id'];

            try{
                $product = Product::query()
                    ->whereCode($productID)
                    ->firstOrFail();

                $order->products()->attach($product->id,
                    [
                        'quantity' => $itemData['quantity'],
                        'price' => $product->price * $itemData['quantity'],
                    ]);
            }
            catch(ModelNotFoundException $ex){
                Log::error('Product '.$productID.' not found for order '.$orderData['id']);

                return null;
            }
        }

        $order->calculateTotalAmountOnOriginalOrder();

        $order->processDiscountOnSingleProducts();
        $order->calculateTotalAmountOfProductDiscounts();

        $order->processDiscountsOnTotalOrder();

    }

    /**
     *
     * Calculate the total amount of the order (without discounts)
     *
     * @return int
     */

    private function calculateTotalAmountOnOriginalOrder(){

        $total = 0;

        $productsOnOrder = $this->products()->get();

        foreach($productsOnOrder as $product){
            $total += $product->price * $product->pivot->quantity;
        }

        $this->setAttribute('total',$total);

        return $total;
    }

    /**
     *
     * Calculate the total amount of the order (considering the discounts applied)
     *
     * @return int
     */

    private function calculateTotalAmountOfProductDiscounts(){

        $total = 0;

        $productsOnOrder = $this->products()->get();

        foreach($productsOnOrder as $product){
            $total += $product->pivot->discount_value;
        }

        $this->setAttribute('discount_value',$total);

        return $total;
    }

    /**
     *
     * Calculate the total amount of items on the order (without discounts)
     *
     * @return int
     */

    private function calculateTotalNumberOfProductsOnOriginalOrder(){

        $total = 0;

        $productsOnOrder = $this->products()->get();

        foreach($productsOnOrder as $product){
            $total += $product->pivot->quantity;
        }

        return $total;
    }

    /**
     *
     * Calculate the total amount of items on the order (with discounts)
     *
     * @return int
     */

    private function calculateTotalNumberOfProductsOnOrderWithDiscounts(){

        $total = 0;

        $productsOnOrder = $this->products()->get();

        foreach($productsOnOrder as $product){
            $total += $product->pivot->quantity + $product->pivot->extra_products;
        }

        return $total;
    }

    /**
     * Returns the customer associated to this order
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function customer()
    {
        return $this->belongsTo('App\Customer');
    }

    /**
     * Get the products associated to this order
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function products()
    {
        return $this->belongsToMany('App\Product')
            ->withPivot('quantity','price','discount_value','extra_products');
    }

    /**
     * Get the discounts associated to this order (global order discounts, not individual products)
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function discounts()
    {
        return $this->belongsToMany('App\Discount');
    }
}
