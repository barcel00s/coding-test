<?php

namespace Tests\Feature;

use Illuminate\Support\Facades\Log;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class DiscountTest extends TestCase
{

    /**
     * A basic test example.
     *
     * @return void
     */
    public function testExample()
    {
        $response = $this->get('/');

        $response->assertStatus(200);
    }


    public function testCheckIfOrderHasDiscounts(){

        $order1 = '[{"id": "1","customer-id": "1","items": [{"product-id": "B102","quantity": "10","unit-price": "4.99",  "total": "49.90"}],"total": "49.90"}]';

        $response = $this->postJson('/orders',[
            'json' => $order1
        ]);

        $response
            ->assertStatus(201)
            ->assertJson([
                'total-with-discounts' => true,
            ]);
    }
}
